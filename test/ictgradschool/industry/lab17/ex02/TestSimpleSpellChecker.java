package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * TODO Implement this class.
 */
public class TestSimpleSpellChecker {
    private String toCheck = "This is the test text that should be used for a test or multiple tests";
    private SimpleSpellChecker checker;
    @Before
    public void setUp(){
        try {
            checker = new SimpleSpellChecker(new Dictionary(), toCheck);
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetMisspelledWords() {
        assertEquals("is", checker.getMisspelledWords().get(0));
        assertEquals("tests", checker.getMisspelledWords().get(checker.getMisspelledWords().size() - 1));
    }

    @Test
    public void testGetUniqueWords(){
        assertEquals("a", checker.getUniqueWords().get(0));
        assertEquals("text", checker.getUniqueWords().get(checker.getUniqueWords().size() -1));
    }

    @Test
    public void testGetFrequencyOfWord() throws InvalidDataFormatException {
        assertEquals(0, checker.getFrequencyOfWord("hi"));
        assertEquals(2, checker.getFrequencyOfWord("test"));
    }

    @Test
    public void testCheckStringNotNull(){
            try {
                checker = new SimpleSpellChecker(new Dictionary(), "Here is a new string");
            } catch (InvalidDataFormatException e) {
                e.printStackTrace();
            }
    }

    @Test
    public void testInvalidDataFormatException(){
        try {
            checker = new SimpleSpellChecker(new Dictionary(), "");
        } catch (InvalidDataFormatException e) {
            //e.printStackTrace();
            assertEquals("Words to check should not be null", e.getMessage());
        }
    }
}