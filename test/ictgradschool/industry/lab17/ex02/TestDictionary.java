package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Implement this class.
 */
public class TestDictionary {
    private Dictionary dict;

    @Before
    public void setUp(){
        dict = new Dictionary();
    }

    @Test
    //testing that instance of Dictionary has words at the expected places in its TreeSet?
    public void testConstructor(){
        //assertEquals("the", dict.); //Can't access the TreeSet becuase it's private...
        assertTrue(dict.isSpellingCorrect("test"));
        assertFalse(dict.isSpellingCorrect("Mengjie"));
    }

    @Test
    public void testIsSpellingCorrectValidWord() {
        assertEquals(true, dict.isSpellingCorrect("the"));
    }

    @Test
    public void testIsSpellingCorrectInvalidWord() {
        assertEquals(false, dict.isSpellingCorrect("Gayoung"));
    }

}