package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveEast() {
        boolean atRight = false;

        //facing east
        myRobot.turn();

        try {
            // Move the robot to the right column.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the right.
            atRight = myRobot.currentState().column == 10;
            assertTrue(atRight);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().column);
        }
    }

    @Test
    public void testIllegalMoveSouth() {
        boolean atBottom = false;

        //facing south
        myRobot.turn();
        myRobot.turn();

        try {
            // Now try to continue to move south
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveWest() {
        boolean atBottom = false;

        //facing west
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().column);
        }
    }

    @Test
    public void testTurnEast(){
        int currentRow = myRobot.currentState().row;
        int currentColumn = myRobot.currentState().column;
        myRobot.turn();
        assertEquals(Robot.Direction.East, myRobot.getDirection());
        assertEquals(currentRow, myRobot.currentState().row);
        assertEquals(currentColumn, myRobot.currentState().column);
    }

    @Test
    public void testTurnSouth(){
        int currentRow = myRobot.currentState().row;
        int currentColumn = myRobot.currentState().column;
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.South, myRobot.getDirection());
        assertEquals(currentRow, myRobot.currentState().row);
        assertEquals(currentColumn, myRobot.currentState().column);
    }

    @Test
    public void testTurnWest(){
        int currentRow = myRobot.currentState().row;
        int currentColumn = myRobot.currentState().column;
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.West, myRobot.getDirection());
        assertEquals(currentRow, myRobot.currentState().row);
        assertEquals(currentColumn, myRobot.currentState().column);
    }

    @Test
    public void testTurnNorth(){
        int currentRow = myRobot.currentState().row;
        int currentColumn = myRobot.currentState().column;
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(currentRow, myRobot.currentState().row);
        assertEquals(currentColumn, myRobot.currentState().column);
    }

    @Test
    public void testMoveNorth(){
        int currentRow = myRobot.currentState().row;
        int currentColumn = myRobot.currentState().column;
        Robot.Direction currentDirection = myRobot.getDirection();
        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
        }
        assertEquals(currentRow - 1, myRobot.currentState().row);
        assertEquals(currentColumn, myRobot.currentState().column);
        assertEquals(currentDirection, myRobot.getDirection());
    }

    @Test
    public void testMoveEast(){
        int currentRow = myRobot.currentState().row;
        int currentColumn = myRobot.currentState().column;

        //face east
        myRobot.turn();
        Robot.Direction currentDirection = myRobot.getDirection();
        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
        }
        assertEquals(currentColumn + 1, myRobot.currentState().column);
        assertEquals(currentRow, myRobot.currentState().row);
        assertEquals(currentDirection, myRobot.getDirection());
    }

    @Test
    public void testMoveSouth(){
        int currentRow = myRobot.currentState().row;
        int currentColumn = myRobot.currentState().column;
        //move up one
        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
        }
        //face south
        myRobot.turn();
        myRobot.turn();
        Robot.Direction currentDirection = myRobot.getDirection();
        try {
            //move down one
            myRobot.move();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
        }
        assertEquals(currentRow, myRobot.currentState().row);
        assertEquals(currentColumn, myRobot.currentState().column);
        assertEquals(currentDirection, myRobot.getDirection());
    }

    @Test
    public void testMoveWest(){
        int currentRow = myRobot.currentState().row;
        int currentColumn = myRobot.currentState().column;

        //face east and move once
        myRobot.turn();
        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
        }
        //face west
        myRobot.turn();
        myRobot.turn();

        Robot.Direction currentDirection = myRobot.getDirection();
        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
        }
        assertEquals(currentRow, myRobot.currentState().row);
        assertEquals(currentColumn , myRobot.currentState().column);
        assertEquals(currentDirection, myRobot.getDirection());
    }

    @Test
    public void testBacktrackMove(){
        int currentRow = myRobot.row();
        int currentColumn = myRobot.column();
        Robot.Direction currentDirection = myRobot.getDirection();

        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            e.printStackTrace();
        }

        myRobot.backTrack();

        assertEquals(currentRow, myRobot.row());
        assertEquals(currentColumn, myRobot.column());
        assertEquals(currentDirection, myRobot.getDirection());
    }

    @Test
    public void testBacktrackTurn(){
        int currentRow = myRobot.row();
        int currentColumn = myRobot.column();
        Robot.Direction currentDirection = myRobot.getDirection();

        myRobot.turn();

        myRobot.backTrack();

        assertEquals(currentRow, myRobot.row());
        assertEquals(currentColumn, myRobot.column());
        assertEquals(currentDirection, myRobot.getDirection());
    }

}
